package lab_02;
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TestGUI {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 300;
	JFrame frame = new JFrame("Testing Color");
	JPanel panel_color;
	JPanel panel_choice;
	JRadioButton red;
	JRadioButton green;
	JRadioButton blue;
	
	public void createWindow(){

	panel_color = new JPanel();
	panel_choice = new JPanel();
	panel_color.setBorder(BorderFactory.createLineBorder(Color.GRAY));
	panel_choice.setBorder(BorderFactory.createLineBorder(Color.GRAY));
	
	
	panel_color.setBackground(Color.WHITE);
	panel_choice.setBackground(Color.WHITE);
	panel_choice.setLayout(new GridLayout(1,3,0,12));
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setVisible(true);
	frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	frame.add(panel_color,BorderLayout.CENTER);
	frame.add(panel_choice,BorderLayout.SOUTH);
	}
	
	public void setButton(){
		red = new JRadioButton("Red");
		green = new JRadioButton("Green");
		blue = new JRadioButton("Blue");
		panel_choice.add(red);
		panel_choice.add(green);
		panel_choice.add(blue);
		
		red.addActionListener ( new ActionListener ( ) 
	     { 
	          @Override 
	          public void actionPerformed ( ActionEvent event ) 
	          { 
	              panel_color.setBackground(Color.RED);; 
	          } 
	     } 
	);
		
		blue.addActionListener ( new ActionListener ( ) 
	     { 
	          @Override 
	          public void actionPerformed ( ActionEvent event ) 
	          { 
	              panel_color.setBackground(Color.BLUE);; 
	          } 
	     } 
	);
		
		green.addActionListener ( new ActionListener ( ) 
	     { 
	          @Override 
	          public void actionPerformed ( ActionEvent event ) 
	          { 
	              panel_color.setBackground(Color.GREEN);; 
	          } 
	     } 
	);
	}
	
	
	public void setPlay(){
		createWindow();
		setButton();
	}
	
	
}

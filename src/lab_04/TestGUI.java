package lab_04;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TestGUI {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 300;
	JFrame frame = new JFrame("Testing Color");
	JPanel panel_color;
	JPanel panel_choice;
	JComboBox clist;
	ActionListener lstner;

	public void createWindow() {

		panel_color = new JPanel();
		panel_choice = new JPanel();
		panel_color.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		panel_choice.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		panel_color.setBackground(Color.WHITE);
		panel_choice.setBackground(Color.WHITE);
		panel_choice.setLayout(new FlowLayout());

		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.add(panel_color, BorderLayout.CENTER);
		frame.add(panel_choice, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void setButton() {
		// Create the combo box, select item at index 4.
		// Indices start at 0, so 4 specifies the pig.
		clist = new JComboBox();
		clist.addItem("Red");
		clist.addItem("Green");
		clist.addItem("Blue");

		lstner = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (clist.getSelectedItem() == "Red") {
					panel_color.setBackground(Color.RED);
				} else if (clist.getSelectedItem() == "Green") {
					panel_color.setBackground(Color.GREEN);
				}
				else if (clist.getSelectedItem() == "Blue") {
					panel_color.setBackground(Color.BLUE);

				}

			}
		};

		clist.addActionListener(lstner);
		panel_choice.add(clist);

	}

	public void setPlay() {
		createWindow();
		setButton();

	}

}

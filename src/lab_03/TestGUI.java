package lab_03;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TestGUI {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 300;
	JFrame frame = new JFrame("Testing Color");
	JPanel panel_color;
	JPanel panel_choice;
	JCheckBox red;
	JCheckBox green;
	JCheckBox blue;
	ActionListener lstner;

	public void createWindow() {

		panel_color = new JPanel();
		panel_choice = new JPanel();
		panel_color.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		panel_choice.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		panel_color.setBackground(Color.GRAY);
		panel_choice.setBackground(Color.WHITE);
		panel_choice.setLayout(new GridLayout(1, 3, 0, 12));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.add(panel_color, BorderLayout.CENTER);
		frame.add(panel_choice, BorderLayout.SOUTH);
	}

	public void setButton() {
		red = new JCheckBox("Red");
		green = new JCheckBox("Green");
		blue = new JCheckBox("Blue");
		
		lstner = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panel_color.setBackground(Color.GRAY);
				if (red.isSelected()) {
					panel_color.setBackground(Color.RED);
				}
				else if (blue.isSelected()){
					panel_color.setBackground(Color.BLUE);
				}
				else if (green.isSelected()){
					panel_color.setBackground(Color.GREEN);
				}
				if( (red.isSelected() && blue.isSelected()) || (blue.isSelected() && red.isSelected()) ) {
					panel_color.setBackground(Color.MAGENTA);
				}
				if( (red.isSelected() && green.isSelected()) || (green.isSelected() && red.isSelected()) ) {
					panel_color.setBackground(Color.YELLOW);
				}
				if( (blue.isSelected() && green.isSelected()) || (green.isSelected() && blue.isSelected()) ) {
					panel_color.setBackground(Color.CYAN);
				}
				if ((blue.isSelected() && green.isSelected()) && red.isSelected()) {
					panel_color.setBackground(Color.WHITE);
			
				}

			}
		};
		
		
		red.addActionListener(lstner);
		green.addActionListener(lstner);
		blue.addActionListener(lstner);
		panel_choice.add(red);
		panel_choice.add(green);
		panel_choice.add(blue);
	
	}

	public void setPlay() {
		createWindow();
		setButton();
	}

}

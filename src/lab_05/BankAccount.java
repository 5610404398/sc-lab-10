package lab_05;

public class BankAccount {
	private double balance;
	private String account;
	
	public BankAccount(String name){
		this.account = name;
		this.balance = 0;
	}
	
	public void deposit(double m){
		this.balance += m;
	}
	
	public void withdraw(double m){
		this.balance -= m;
	}
	
	public double getBalance(){
		return balance;
	}
	
	public String getAccount(){
		return account;
	}
	
}

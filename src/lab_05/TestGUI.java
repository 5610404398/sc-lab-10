package lab_05;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TestGUI {
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 120;
	BankAccount baitoey = new BankAccount("Baitoey");;
	JFrame frame = new JFrame("Testing Color");
	JPanel panel_color;
	JPanel panel_choice;
	JPanel panel_in;
	JLabel text;
	JButton w;
	JButton d;
	ActionListener lstner;
	JTextField input;
	JLabel ans;

	public void createWindow() {
		panel_color = new JPanel();
		panel_choice = new JPanel();
		panel_in = new JPanel();
		panel_in.setLayout(new GridLayout(1, 2));
		panel_color.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		panel_choice.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		panel_color.setBackground(Color.WHITE);
		panel_choice.setBackground(Color.WHITE);
		panel_choice.setLayout(new GridLayout(1,2));
	
		
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.add(panel_color, BorderLayout.NORTH);
		frame.add(panel_in, BorderLayout.CENTER);
		frame.add(panel_choice, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void setButton() {
		input = new JTextField();
		ans = new JLabel("   Balance : 0 � ");
		text = new JLabel("    Please input amount ::");
		text.setLocation(30, 0);
		input.setSize(100, 10);
		w = new JButton("Withdraw");
		d = new JButton("Deposite");
		panel_color.add(ans);
		panel_choice.add(d);
		panel_choice.add(w);
		panel_in.add(text);
		panel_in.add(input);
		
		w.addActionListener ( new ActionListener ( ) 
	     { 
	          @Override 
	          public void actionPerformed ( ActionEvent event ) 
	          { 
	              panel_color.setBackground(Color.RED);; 
	              int number = Integer.parseInt(input.getText());
	              baitoey.withdraw(number);
	              ans.setText("   Balance : "+baitoey.getBalance()+" � ");
	          } 
	     } 
	);
		
		d.addActionListener ( new ActionListener ( ) 
	     { 
	          @Override 
	          public void actionPerformed ( ActionEvent event ) 
	          { 
	              panel_color.setBackground(Color.GREEN);; 
	              int number = Integer.parseInt(input.getText());
	              baitoey.deposit(number);
	              ans.setText("   Balance : "+baitoey.getBalance()+" � ");
	          } 
	     } 
	);
		
			
	}

	public void setPlay() {
		createWindow();
		setButton();

	}

}
